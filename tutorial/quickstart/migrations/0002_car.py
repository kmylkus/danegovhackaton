# Generated by Django 2.0.7 on 2018-10-13 12:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quickstart', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Car',
            fields=[
                ('car_id', models.BigIntegerField(primary_key=True, serialize=False)),
                ('car_name', models.CharField(blank=True, max_length=255, null=True)),
                ('petrol', models.CharField(blank=True, max_length=255, null=True)),
                ('price', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'car',
                'managed': False,
            },
        ),
    ]
