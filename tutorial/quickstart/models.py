from django.db import models


class Car(models.Model):
    car_id = models.BigIntegerField(primary_key=True)
    car_name = models.CharField(max_length=255, blank=True, null=True)
    petrol = models.CharField(max_length=255, blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    # def __str__(self):
    #         return self.app_name




