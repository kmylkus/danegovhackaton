from django.contrib.auth.models import User, Group
from rest_framework import viewsets, views
from quickstart.serializers import UserSerializer, GroupSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
import csv
import io
import json
import os.path
import numpy as np
import pandas as pd
from glob import glob

from parsecsv import parsecsv

resource_df = pd.read_csv("/data/datagov/resources.csv", index_col=0).set_index("id")

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # df = pd.read_csv('C:/Users/KMylkus/Desktop/test.csv')
    #print(df)


    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

class UserCountView(APIView):
    """
    A view that returns the count of active users in JSON.
    """
    renderer_classes = (JSONRenderer, )

    def get(self, request, format=None):
        user_count = User.objects.count()
        content = {'user_count': user_count}
        return Response(content)


def load_csv(csv_id, for_json=False):
    path_to_csv = f'/data/resources_processed/{csv_id}.csv'
    header_rows = 0
    if os.path.isfile(path_to_csv):
        if for_json:
            (df_header, df), _ = parsecsv.parse_csv(path_to_csv, header_rows=0)
            if df_header is not None:
                header_rows = len(df_header)
        else:
            (df_header, df), _ = parsecsv.parse_csv(path_to_csv, header_rows=1)
            df_header = df_header.iloc[0].str.split("\|>\|", expand=True).T
            header_rows = len(df_header)
            df = pd.concat([df_header, df], ignore_index=False)
            df_header = None
    else: 
        path_to_csv = f'/data/resources/{csv_id}.csv'
        (df_header, df), _ = parsecsv.parse_csv(path_to_csv)
        if df_header is not None:
            header_rows = len(df_header)

    if df_header is not None:
        df = df_header.iloc[:,:df.shape[1]].append(df)
    return df, header_rows

class ValidatorView(views.APIView): 
    def get(self, request, format=None):
        csv_id = request.GET.get('plik')
        df, header_rows = load_csv(csv_id)
        warnings = parsecsv.detectNone(df.iloc[header_rows:].reset_index(drop=True))
        warnings = sorted(warnings, key=lambda x: (x['row'], x['col']))
        for warn in warnings:
            warn['row'] += header_rows
        args = {
            'warnings': warnings 
        }

        return Response(args)


class CSVView(views.APIView): 
    def get(self, request, format=None):
        csv_id = request.GET.get('plik')
        df, header_rows = load_csv(csv_id, for_json=True)
        # warnings = parsecsv.detectNone(df)
        
        dane = df.to_json(orient='table')
        dane = json.loads(dane)
        
        args = {
            'dane': dane,
            # 'warnings': warnings 
        }
        return Response(args)

    def post(self, request):
        dane_json = request.data
        args = {'dane_json': dane_json}
        return Response(args)
    
    # def save_data(request):
    #     if request.method == 'POST':
    #         json_data = json.loads(request.body) # request.raw_post_data w/ Django < 1.4
    #     try:
    #         data = json_data['data']
    #     except KeyError:
    #         HttpResponseServerError("Malformed data!")
    #     HttpResponse("Got json data")

def list_csv(request):
    from os.path import basename
    csv_list = [basename(f)[:-4] for f in glob('/data/resources/*.csv')]
    template = loader.get_template('list.html')
    # csv_meta = {str(csv_id): (r.title, r.description) for csv_id, r in resource_df.iteritems()}
    context = {
        "csv_list": csv_list,
        # 'csv_meta': csv_meta,
    }
    return HttpResponse(template.render(context, request))


def view_csv(request, csv_id):
    df, header_rows = load_csv(csv_id)
    template = loader.get_template('view.html')
    res_meta = resource_df.loc[int(csv_id)]
    context = {
        "data": df.to_json(orient='values'),
        "title": res_meta['title'],
        "description": res_meta['description'],
        "csv_id": csv_id,
    }
    return HttpResponse(template.render(context, request))


def save_csv(request, csv_id):
    data = request.POST['csv']
    header_rows = int(request.POST['header_rows'])
    
    df = pd.read_json(data)
    
    df_header = df.iloc[:header_rows]
    df = df.iloc[header_rows:]
    header = df_header.T.fillna("").apply(lambda x:"|>|".join(x), axis=1)
    df = pd.concat([pd.DataFrame(header).T, df], ignore_index=True)
    print(df)

    df.to_csv(f"/data/resources_processed/{csv_id}.csv", header=False, index=False)

    return HttpResponseRedirect(f"/csv/{csv_id}/view/")
    

def delete_csv(request, csv_id):
    path_to_csv = f'/data/resources_processed/{csv_id}.csv'
    if os.path.isfile(path_to_csv): 
        os.remove(path_to_csv)
    return HttpResponseRedirect(f"/csv/{csv_id}/view/")