from django.conf.urls import url, include
from rest_framework import routers
from quickstart import views
from django.contrib import admin
from django.urls import include, path

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
    path('csv/', views.list_csv),
    path('dane_csv/', views.CSVView.as_view()),
    path('csv_validate/', views.ValidatorView.as_view()),
    path('csv/<str:csv_id>/view/', views.view_csv),
    path('csv/<str:csv_id>/save/', views.save_csv),
    path('csv/<str:csv_id>/delete/', views.delete_csv),
]

