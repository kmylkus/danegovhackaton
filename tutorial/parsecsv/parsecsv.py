import csv
import magic
import io
import numpy as np
import pandas as pd

DEFAULT_ENCODING = 'windows-1250'
MAGIC_UNKNOW_ENCODING = 'unknown-8bit'

NUMBER_RE = r'^-?[\d\. ,]+(%|(zł)|(zl))?$'


def detect_encoding(data: bytes):
    m = magic.Magic(mime_encoding=True)
    encoding = m.from_buffer(data)
    encoding = DEFAULT_ENCODING if encoding == MAGIC_UNKNOW_ENCODING else encoding
    return encoding


def detect_dialect(data: str):
    sniffer = csv.Sniffer()
    dialect = sniffer.sniff(data)
    return dialect


class DelimiterAttributes():
    def __init__(self, csvfile: io.TextIOWrapper, delimiter):
        self._inlines = np.array([sum(1 for char in line if char == ord(delimiter))
                                  for line in csvfile])
        self.cols_max = self._inlines.max() + 1
        self.cols_median = int(np.median(self._inlines)) + 1

    def skip_rows(self, thresh=0.9):
        skip = 0
        for num_cols in (self._inlines + 1) / self.cols_median:
            if num_cols > 0.9:
                break
            skip += 1
        return skip


def detect_header_lines(df: pd.DataFrame):
    """Detect header lines in a dataframe based on number counts in a row. 
       If row has less number than median number per row, assume it's header row"""
    numbers_per_row = np.array([row.fillna(0).astype('object').str.match(
        NUMBER_RE).sum() / df.shape[1] for _, row in df[:30].iterrows()])
    if numbers_per_row.sum() == 0:
        return 0
    median_nums_per_row = np.median(numbers_per_row[numbers_per_row > 0])
    print(median_nums_per_row, numbers_per_row)
    header_lines = 0
    for ix, nums_in_row in enumerate(numbers_per_row):
        print(nums_in_row, numbers_per_row[max(ix-1, 0)], (nums_in_row >= median_nums_per_row) )
        if (nums_in_row >= median_nums_per_row) or (nums_in_row - numbers_per_row[max(ix-1, 0)] > 0.2):
            break
        header_lines += 1
    return header_lines


def count_numbers_in_cols(df: pd.DataFrame):
    numers_cols = {}
    for col in df.select_dtypes(include=['object']).columns:
        col_data = df[col].dropna()
        numers_cols[col] = col_data.str.match(NUMBER_RE).sum() / col_data.shape[0]
    return numers_cols


def try_cols2numbers(df: pd.DataFrame):
    for col in df:
        if df[col].dtype == 'object':
            try:
                df[col] = pd.to_numeric(df[col].str.replace(" ", ""))
            except Exception as e:
                pass


def precheck_sample(filename, max_rows=False):
    with open(filename, "rb") as csvfile:
        file_sample = csvfile.read(2048 * 4)
        csvfile.seek(0)

        encoding = detect_encoding(file_sample)
        file_sample = file_sample.decode(encoding)
        dialect = detect_dialect(file_sample) 

        lines_count = sum(1 for line in csvfile)
        csvfile.seek(0)

        if max_rows and lines_count > max_rows:
            raise "To many rows"

        delim_attrs = DelimiterAttributes(csvfile, dialect.delimiter)
        skiprows = delim_attrs.skip_rows()
        csvfile.seek(0)

    return encoding, dialect, lines_count, skiprows


def remove_empty_rows(df):
    # remove rows with only one value
    rows_with_more_than_one_value = ((~df.isnull()).sum(axis=1) > 1)
    empty_rows_ix = (~rows_with_more_than_one_value).nonzero()[0].tolist()
    # only first rows in the header
    empty_rows_ix = np.flatnonzero(empty_rows_ix == np.arange(len(empty_rows_ix))) 
    is_one_column_csv = df.shape[1] < 2
    if is_one_column_csv:
        empty_rows_ix = []
    df = df[~df.index.isin(empty_rows_ix)]
    return df


def parse_csv(filename, max_rows=False, header_rows=None):
    encoding, dialect, lines_count, skiprows = precheck_sample(filename, max_rows=max_rows)

    file_attributes = {}
    file_attributes['encoding'] = encoding
    # file_attributes['dialect'] = dialect
    file_attributes['delimiter'] = dialect.delimiter
    file_attributes['lines_count'] = dialect.delimiter
    file_attributes['rows_skip'] = skiprows

    if header_rows is not None:
        skiprows = 0

    df = pd.read_csv(filename, sep=dialect.delimiter, encoding=encoding,
                    header=None, thousands=" ", decimal=',',
                    skip_blank_lines=False, skiprows=skiprows, error_bad_lines=False)
    
    if header_rows is None:
        # Try converting to numbers again
        try_cols2numbers(df)

        # detect header lines 
        # TODO Score headers based on num to string ratio (higher ratio, higher probablity of correct parsing)
        # TODO Apply same thing for footer
        header_rows = detect_header_lines(df)

    df_header = None
    if header_rows > 0:
        df_header = df.iloc[:header_rows]
        df = pd.read_csv(filename, sep=dialect.delimiter, encoding=encoding,
                        header=None, thousands=" ", decimal=',',
                        skip_blank_lines=False, skiprows=skiprows+header_rows, error_bad_lines=False)
        df = df.dropna(how="all").reset_index(drop=True)
        df_header = df_header.dropna(how="all").reset_index(drop=True)

    # remove empty columns
    empty_columns_ix = (df.isnull().sum(axis=0) == df.shape[0]).nonzero()[0].tolist()
    file_attributes['empty_columns'] = empty_columns_ix
    df = df.dropna(axis=1, how='all').dropna(how="all").reset_index(drop=True)

    if df_header is not None:
        df_header = remove_empty_rows(df_header)
    else:
        df = remove_empty_rows(df)

    file_attributes['header_rows'] = len(df_header) if df_header is not None else 0


    # index cols happens as first df value
    if (pd.to_numeric(df.iloc[0], errors='ignore').values == np.arange(df.shape[1])).all():
        df = df.iloc[1:]

    # Try converting to numbers again
    try_cols2numbers(df)

    # file_attributes['col_number_ratio'] = count_numbers_in_cols(df)

    return (df_header, df), file_attributes


def detectNone(df):
   data = []
   i = 0
   headers= list(df.select_dtypes('object').columns.values)
   while i < len(headers):
       x= df[headers[i]].str.isnumeric()
       d =x.value_counts()
       noneValues = np.flatnonzero(df[headers[i]].isna())
       if True not in d or d[False] > d[True]:
           anotherValues = np.flatnonzero(x)
       else:
           anotherValues = np.flatnonzero(np.logical_not(x))

       n=0
       while n < len(noneValues):
           d = ({'row': noneValues[n], 'col': i, 'warn': 'null_value'})
           data.append(d )
           n= n+1
       anotherValues = np.setdiff1d(anotherValues, noneValues)

       k=0
       while k < len(anotherValues) :
           d = ({'row': anotherValues[k], 'col': i, 'warn': 'not_matching_type'})
           data.append(d )
           k= k+1
       i= i+1
   return data


if __name__ == '__main__':
    import glob
    from tqdm import tqdm
    from joblib import Parallel, delayed, dump

    read_stats = []
    dfs = {}
    err_files = []

    def parse_file(filename):
        try:
            df, file_attributes = parse_csv(filename, max_rows=10000)
            file_attributes['filename'] = filename
            return filename, df, file_attributes
        except Exception as e:
            # err_files.append((filename, e))
            print(filename, e)

    files = glob.glob("./data/resources/*.csv")
    results = Parallel(n_jobs=4, verbose=10)(delayed(parse_file)(filename) for filename in files)
    results = list(filter(None, results))
    for filename, df, file_attributes in results:
        dfs[filename] = df
        read_stats.append(file_attributes)
            
    print("Parsed files:", len(dfs))
    print("Files with errors:", len(files) - len(dfs))
    print("Parsed with headers:", sum([1 for df in dfs.values() if df and df[0] is None]))
    
    dump((dfs, read_stats), "./data/datagov/dfs.pkl")

